#DevOps Essentials Assignment

Create a small application in which you can practice the whole CI/CD implementation.

What should be submitted?
- A link to a GitLab CI repo where we can browse and evaluate the code.
> https://gitlab.com/joao.gomes4/ci-cd-assignment


- A link to the docker images pushed to the Docker Hub.
> https://hub.docker.com/r/joaogomesavenue/ci-cd-assignment/tags?page=1&ordering=last_updated

#Databases Essentials Assignment 30/08
- Using mongoDB cluster (see application.properties file)
- Use:
  - GET METHOD http://localhost:8080/all (Get all Users)
  - POST METHOD http://localhost:8080/{name} (Insert new User)

NOTE: You can use 
> docker run -p 8080:8080 joaogomesavenue/ci-cd-assignment

To run this code locally.

#Test Automation Assignment 31/08

Run the tests with 
> nvm test

- UserServiceTest - Unit tests
- UserControllerTest - Integration tests
- UserControllerAcceptanceTest - Acceptance tests
