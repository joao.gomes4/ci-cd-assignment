package com.example.cicdassignment.service;

import com.example.cicdassignment.model.User;
import com.example.cicdassignment.repository.IUserRepository;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Data
public class UserService {

    private final IUserRepository repository;

    public User saveUser(String name){
        Optional<User> found = getRepository().findByName(name);
        if (found != null && found.isPresent()){
            return null;
        }
        User user = new User(null, name);
        return getRepository().save(user);
    }

    public List<User> getAllUsers(){
        return  getRepository().findAll();
    }
}
