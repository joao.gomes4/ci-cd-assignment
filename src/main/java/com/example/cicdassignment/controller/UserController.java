package com.example.cicdassignment.controller;

import com.example.cicdassignment.model.User;
import com.example.cicdassignment.service.UserService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@Data
public class UserController {

    private final UserService service;

    @GetMapping
    public ResponseEntity<String> index(){
        return ResponseEntity.ok().body("Hello guest, use <strong>/{name}</strong> with post method to save some users. " +
                "<p>And <strong>/all</strong> with get method to view all users.</p>");
    }

    @PostMapping("/{name}")
    public ResponseEntity<Void> saveUser(@PathVariable String name){
        User user = getService().saveUser(name);
        if (user != null)
            return ResponseEntity.status(HttpStatus.CREATED).build();
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll(){
        return ResponseEntity.ok().body(getService().getAllUsers());
    }
}
