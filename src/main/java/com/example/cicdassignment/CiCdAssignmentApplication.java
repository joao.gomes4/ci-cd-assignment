package com.example.cicdassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiCdAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiCdAssignmentApplication.class, args);
	}

}
