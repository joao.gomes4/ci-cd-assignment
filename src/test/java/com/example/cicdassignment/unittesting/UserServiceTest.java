package com.example.cicdassignment.unittesting;

import com.example.cicdassignment.model.User;
import com.example.cicdassignment.repository.IUserRepository;
import com.example.cicdassignment.service.UserService;
import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static java.util.Optional.of;
import static org.junit.Assert.*;

@Data
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Configuration
    static class ContextConfig{

        @MockBean
        private IUserRepository repository;

        @Bean
        public UserService createContextConfig(){
            return new UserService(repository);
        }
    }

    @Autowired
    private UserService userService;

    private final String JOAO = "Joao";

    @Test
    public void addUserSuccessTest() {

        Mockito.when(getUserService().getRepository().save(Mockito.any())).thenReturn(new User("1234",JOAO));
        Mockito.when(getUserService().getRepository().findByName(Mockito.any())).thenReturn(null);
        User user = getUserService().saveUser(JOAO);

        assertNotNull(user);
        assertEquals(JOAO, user.getName());
    }

    @Test
    public void addUserFailTest() {

        Mockito.when(getUserService().getRepository().save(Mockito.any())).thenReturn(new User("1234",JOAO));
        Mockito.when(getUserService().getRepository().findByName(Mockito.any())).thenReturn(of(new User("", "")));
        User user = getUserService().saveUser(JOAO);

        assertNull(user);
    }

    @Test
    public void getAllUsersTest() {

        Mockito.when(getUserService().getRepository().findAll()).thenReturn(Arrays.asList(new User("1234",JOAO)));
        List<User> users = getUserService().getAllUsers();

        assertNotNull(users);
        assertEquals(1, users.size());
    }
}
