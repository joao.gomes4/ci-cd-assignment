package com.example.cicdassignment.acceptancetesting.glue;

import com.example.cicdassignment.model.User;
import com.example.cicdassignment.repository.IUserRepository;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.Optional;

import static org.junit.Assert.*;

public class UserSteps {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private IUserRepository repository;

    private String expectedIndexPage;
    private String actualIndexPage;

    @Before
    public void setup(){
        repository.deleteAll();
    }

    @Given("^the index page$")
    public void givenTheIndexPage(final String indexPage){
        expectedIndexPage = indexPage;
    }

    @When("^requests index page$")
    public void whenRequestsIndexPage(){
        actualIndexPage = testRestTemplate.getForEntity("/", String.class).getBody();
    }

    @Then("^the index page is returned$")
    public void thenIndexPageIsReturned(){

        assertEquals(expectedIndexPage, actualIndexPage);
    }

    @When("^requests new user with name (.*)$")
    public void whenInsertNewUser(final String name){
        final User expectedUser = new User(null, name);
        testRestTemplate.postForEntity("/"+name,null, Void.class);
    }

    @Then("^(.*) is in database$")
    public void thenIsInDatabase(final String name){
        assertTrue(findUser(name).isPresent());
    }

    @And("^(.*) has an id$")
    public void andHasAnId(final String name){
        assertNotNull(findUser(name).get().getId());
    }

    private Optional<User> findUser(String name) {
        return repository.findAll().stream().filter(user -> user.getName().equals(name)).findFirst();
    }
}
