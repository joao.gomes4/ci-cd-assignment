Feature: User feature
  Scenario: Call the index page
    Given the index page
      | Hello guest, use <strong>/{name}</strong> with post method to save some users. <p>And <strong>/all</strong> with get method to view all users.</p> |
    When requests index page
    Then the index page is returned

  Scenario: Insert new user
    When requests new user with name Toddy
    Then Toddy is in database
    And Toddy has an id